/*
 * rtos_i2c.h
 *
 *  Created on: Oct 13, 2019
 *      Author: Raymundo
 */

#ifndef RTOS_I2C_H_
#define RTOS_I2C_H_

#include <stdint.h>
#include "fsl_i2c.h"
#include "fsl_clock.h"
#include "fsl_port.h"

#include "FreeRTOS.h"
#include "semphr.h"

#define NUMBER_OF_SERIAL_PORTS (3)

typedef enum {rtos_i2c0,rtos_i2c1,rtos_i2c2} rtos_i2c_number_t;
typedef enum {rtos_i2c_portA,rtos_i2c_portB,rtos_i2c_portC,rtos_i2c_portD,rtos_i2c_portE} rtos_i2c_port_t;
typedef enum {rtos_i2c_sucess,rtos_i2c_fail} rtos_i2c_flag_t;

typedef struct
{
	uint32_t  baudrate;
	rtos_i2c_number_t i2c_number;
	rtos_i2c_port_t port;
	uint8_t SCL_pin;
	uint8_t SDA_pin;
	uint8_t pin_mux;
}rtos_i2c_config_t;

typedef struct
{
	uint8_t is_init;
	i2c_master_handle_t fsl_i2c_handle;
	SemaphoreHandle_t mutex_rx;
	SemaphoreHandle_t mutex_tx;
	SemaphoreHandle_t rx_sem;
	SemaphoreHandle_t tx_sem;
}rtos_i2c_handle_t;

rtos_i2c_flag_t rtos_i2c_init(rtos_i2c_config_t config);
rtos_i2c_flag_t rtos_i2c_send(rtos_i2c_number_t i2c_number, uint8_t * buffer, uint16_t lenght, uint16_t subAddress, uint8_t subAddressSize, uint8_t slaveAdress);
rtos_i2c_flag_t rtos_i2c_receive(rtos_i2c_number_t i2c_number, uint8_t * buffer, uint16_t lenght, uint16_t subAddress, uint8_t subAddressSize, uint8_t slaveAdress);

#endif /* RTOS_I2C_H_ */
