/**

This is a GPIO module designed by Darío but Ray is my teamate so he helped too


*/

#ifndef RTOS_GPIO_H_
#define RTOS_GPIO_H_

#include <stdint.h>

/** Enum to know which port is being accessed */
typedef enum {rtos_gpio_portA,rtos_gpio_portB,rtos_gpio_portC,rtos_gpio_portD,rtos_gpio_portE} rtos_gpio_port_t;
/** Success or fail*/
typedef enum {rtos_gpio_sucess,rtos_gpio_fail} rtos_gpio_flag_t;
/** interrupt enables or disables*/
typedef enum {interrupt_enable,interrupt_disable} rtos_gpio_interrupt_flag_t;

/** Gpio config struc*/
typedef struct
{
	rtos_gpio_port_t port;
	uint8_t pin_number;
	uint8_t direction;
	port_pin_config_t pin_config;
	rtos_gpio_interrupt_flag_t interrupt_flag;
	port_interrupt_t interrupt_type;
}rtos_gpio_config_t;

/** Initialization, passes config*/
rtos_gpio_flag_t rtos_gpio_init(rtos_gpio_config_t *config);
/** Puts taks with given config to wait */
rtos_gpio_flag_t rtos_gpio_wait(rtos_gpio_config_t *config);
/** Creates a handler task with the previous config*/
void rtos_create_gpio_handler_task();

#endif /* RTOS_GPIO_H_ */