/*
 * rtos_i2c.c
 *
 *  Created on: Oct 13, 2019
 *      Author: Raymundo
 */

#include "rtos_i2c.h"

static inline void enable_port_clock(rtos_i2c_port_t);
static inline I2C_Type * get_i2c_base(rtos_i2c_number_t);
static inline PORT_Type * get_port_base(rtos_i2c_port_t);
static void fsl_i2c_callback(I2C_Type *base, i2c_master_handle_t *handle, status_t status, void *userData);



static rtos_i2c_handle_t i2c_handles[NUMBER_OF_SERIAL_PORTS] = {0};

static void fsl_i2c_callback(I2C_Type *base, i2c_master_handle_t *handle, status_t status, void *userData)
{
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	if(handle->transfer.direction == kI2C_Write) //To know if we are writing
	{
		if (kStatus_Success == status)
		{
			if(I2C0 == base)
			{
				xSemaphoreGiveFromISR(i2c_handles[rtos_i2c0].tx_sem, &xHigherPriorityTaskWoken );
			}
			else if (I2C1 == base)
			{
				xSemaphoreGiveFromISR(i2c_handles[rtos_i2c1].tx_sem, &xHigherPriorityTaskWoken );
			}
			else
			{
				xSemaphoreGiveFromISR(i2c_handles[rtos_i2c2].tx_sem, &xHigherPriorityTaskWoken );
			}
		}
	}
	else //Else we are reading
	{
		if (kStatus_Success == status)
		{
			if(I2C0 == base)
			{
				xSemaphoreGiveFromISR(i2c_handles[rtos_i2c0].rx_sem, &xHigherPriorityTaskWoken );
			}
			else if(I2C1 == base)
			{
				xSemaphoreGiveFromISR(i2c_handles[rtos_i2c1].rx_sem, &xHigherPriorityTaskWoken );
			}
			else
			{
				xSemaphoreGiveFromISR(i2c_handles[rtos_i2c2].rx_sem, &xHigherPriorityTaskWoken );
			}
		}
	}
	portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}

rtos_i2c_flag_t rtos_i2c_init(rtos_i2c_config_t config)
{
	rtos_i2c_flag_t retval = rtos_i2c_fail;
	i2c_master_config_t fsl_config;

	if(config.i2c_number< NUMBER_OF_SERIAL_PORTS)
	{
		if(!i2c_handles[config.i2c_number].is_init)
		{
			i2c_handles[config.i2c_number].mutex_rx = xSemaphoreCreateMutex();
			i2c_handles[config.i2c_number].mutex_tx = xSemaphoreCreateMutex();

			i2c_handles[config.i2c_number].rx_sem = xSemaphoreCreateBinary();
			i2c_handles[config.i2c_number].tx_sem = xSemaphoreCreateBinary();


			enable_port_clock(config.port);
			PORT_SetPinMux(get_port_base(config.port), config.SCL_pin, config.pin_mux);
			PORT_SetPinMux(get_port_base(config.port), config.SDA_pin, config.pin_mux);

			I2C_MasterGetDefaultConfig(&fsl_config);
			fsl_config.baudRate_Bps = config.baudrate;
			fsl_config.enableMaster = true;


			if(rtos_i2c0 == config.i2c_number)
			{
				I2C_MasterInit(get_i2c_base(rtos_i2c0), &fsl_config, CLOCK_GetFreq(I2C0_CLK_SRC));
				NVIC_SetPriority(I2C0_IRQn,5);
			}
			else if(rtos_i2c1 == config.i2c_number)
			{
				I2C_MasterInit(get_i2c_base(rtos_i2c1), &fsl_config, CLOCK_GetFreq(I2C1_CLK_SRC));
				NVIC_SetPriority(I2C1_IRQn,5);
			}
			else
			{
				I2C_MasterInit(get_i2c_base(rtos_i2c2), &fsl_config, CLOCK_GetFreq(I2C2_CLK_SRC));
				NVIC_SetPriority(I2C2_IRQn,5);
			}

			I2C_MasterTransferCreateHandle(get_i2c_base(config.i2c_number),&i2c_handles[config.i2c_number].fsl_i2c_handle, fsl_i2c_callback, NULL);

			i2c_handles[config.i2c_number].is_init = 1;
			retval = rtos_i2c_sucess;
		}
	}
	return retval;
}

rtos_i2c_flag_t rtos_i2c_send(rtos_i2c_number_t i2c_number, uint8_t * buffer, uint16_t lenght, uint16_t subAddress, uint8_t subAddressSize, uint8_t slaveAdress)
{
	rtos_i2c_flag_t flag = rtos_i2c_fail;
	i2c_master_transfer_t xfer;
	if(i2c_handles[i2c_number].is_init)
	{
		xfer.data= buffer;
		xfer.dataSize = lenght;
		xfer.direction= kI2C_Write;
		xfer.flags = kI2C_TransferDefaultFlag;
		xfer.slaveAddress = slaveAdress;
		xfer.subaddress = subAddress;
		xfer.subaddressSize = subAddressSize;

		xSemaphoreTake(i2c_handles[i2c_number].mutex_tx, portMAX_DELAY);
		I2C_MasterTransferNonBlocking(get_i2c_base(i2c_number), &i2c_handles[i2c_number].fsl_i2c_handle, &xfer);
		xSemaphoreTake(i2c_handles[i2c_number].tx_sem ,portMAX_DELAY);
		xSemaphoreGive(i2c_handles[i2c_number].mutex_tx);
		flag = rtos_i2c_sucess;
	}
	return flag;
}

rtos_i2c_flag_t rtos_i2c_receive(rtos_i2c_number_t i2c_number, uint8_t * buffer, uint16_t lenght, uint16_t subAddress, uint8_t subAddressSize, uint8_t slaveAdress)
{
	rtos_i2c_flag_t flag = rtos_i2c_fail;
	i2c_master_transfer_t xfer;
	if(i2c_handles[i2c_number].is_init)
	{
		xfer.data= buffer;
		xfer.dataSize = lenght;
		xfer.direction= kI2C_Read;
		xfer.flags = kI2C_TransferDefaultFlag;
		xfer.slaveAddress = slaveAdress;
		xfer.subaddress = subAddress;
		xfer.subaddressSize = subAddressSize;

		xSemaphoreTake(i2c_handles[i2c_number].mutex_rx, portMAX_DELAY);
		I2C_MasterTransferNonBlocking(get_i2c_base(i2c_number), &i2c_handles[i2c_number].fsl_i2c_handle, &xfer);
		xSemaphoreTake(i2c_handles[i2c_number].rx_sem,portMAX_DELAY);
		xSemaphoreGive(i2c_handles[i2c_number].mutex_rx);
		flag = rtos_i2c_sucess;
	}
	return flag;
}

static inline void enable_port_clock(rtos_i2c_port_t port)
{
	switch(port)
	{
	case rtos_i2c_portA:
		CLOCK_EnableClock(kCLOCK_PortA);
		break;
	case rtos_i2c_portB:
		CLOCK_EnableClock(kCLOCK_PortB);
		break;
	case rtos_i2c_portC:
		CLOCK_EnableClock(kCLOCK_PortC);
		break;
	case rtos_i2c_portD:
		CLOCK_EnableClock(kCLOCK_PortD);
		break;
	case rtos_i2c_portE:
		CLOCK_EnableClock(kCLOCK_PortE);
		break;
	}
}

static inline I2C_Type * get_i2c_base(rtos_i2c_number_t i2c_number)
{
	I2C_Type * retval = I2C0;
	switch(i2c_number)
	{
	case rtos_i2c0:
		retval = I2C0;
		break;
	case rtos_i2c1:
		retval = I2C1;
		break;
	case rtos_i2c2:
		retval = I2C2;
		break;
	}
	return retval;
}

static inline PORT_Type * get_port_base(rtos_i2c_port_t port)
{
	PORT_Type * port_base = PORTA;
	switch(port)
	{
	case rtos_i2c_portA:
		port_base = PORTA;
		break;
	case rtos_i2c_portB:
		port_base = PORTB;
		break;
	case rtos_i2c_portC:
		port_base = PORTC;
		break;
	case rtos_i2c_portD:
		port_base = PORTD;
		break;
	case rtos_i2c_portE:
		port_base = PORTE;
		break;
	}
	return port_base;
}
