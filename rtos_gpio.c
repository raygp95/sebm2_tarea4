/**

This is a GPIO module designed 
by Darío but Ray is my teamate so he helped too

*/
#include "rtos_gpio.h"

#include "fsl_gpio.h"
#include "fsl_clock.h"
#include "fsl_port.h"
#include "fsl_debug_console.h"

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#define NUMBER_OF_GPIO_HANDLERS (5)

/** Struct to save information about incoming event */

typedef struct
{
	uint8_t current_pin;
	PORT_Type* current_port;
}rtos_gpio_event_t;

/** Struct to know information about port in a given time */

typedef struct
{
	uint8_t is_init;
	uint8_t handle_number;
	rtos_gpio_config_t gpio_config;
	SemaphoreHandle_t interrupt_sem;
}rtos_gpio_handle_t;

/* Functions */

static inline PORT_Type * get_port_base(rtos_gpio_port_t);
static inline clock_ip_name_t get_clock(rtos_gpio_port_t port);
static inline GPIO_Type * get_gpio_base(rtos_gpio_port_t port);

/* initializae events and handles */

static rtos_gpio_handle_t gpio_handles[NUMBER_OF_GPIO_HANDLERS] = {0};
static rtos_gpio_event_t current_event;
static SemaphoreHandle_t gpio_event_sem;

/* For each of these interrupts the handler saes pin and port to know the event,
	then clears the interrupt flag and gives the semaphore */
/** Interrupt handler for port a. This is where sistem goes on an interrupt call */

void PORTA_IRQHandler()
{
	current_event.current_pin = PORT_GetPinsInterruptFlags(PORTA); //Get the pin that generated the interrupt
	current_event.current_port = PORTA;
	PORT_ClearPinsInterruptFlags( PORTA, current_event.current_pin); //Deactivate interrupt
	xSemaphoreGiveFromISR(gpio_event_sem, 0); //Give semaphore to gpio_handler
}

/** Interrupt handler for port b. This is where sistem goes on an interrupt call */

void PORTB_IRQHandler()
{
	current_event.current_pin = PORT_GetPinsInterruptFlags(PORTB); //Get the pin that generated the interrupt
	current_event.current_port = PORTB;
	PORT_ClearPinsInterruptFlags( PORTB, current_event.current_pin); //Deactivate interrupt
	xSemaphoreGiveFromISR(gpio_event_sem, 0); //Give semaphore to gpio_handler
}

/** Interrupt handler for port c. This is where sistem goes on an interrupt call */

void PORTC_IRQHandler()
{
	current_event.current_pin = PORT_GetPinsInterruptFlags(PORTC); //Get the pin that generated the interrupt
	current_event.current_port = PORTC;
	PORT_ClearPinsInterruptFlags( PORTC, current_event.current_pin); //Deactivate interrupt
	xSemaphoreGiveFromISR(gpio_event_sem, 0); //Give semaphore to gpio_handler
}

/** Interrupt handler for port d. This is where sistem goes on an interrupt call */

void PORTD_IRQHandler()
{
	current_event.current_pin = PORT_GetPinsInterruptFlags(PORTD); //Searches the pin that generated the interrupt
	current_event.current_port = PORTD;
	PORT_ClearPinsInterruptFlags( PORTD, current_event.current_pin); //Deactivate interrupt
	xSemaphoreGiveFromISR(gpio_event_sem, 0); //Give semaphore to gpio_handler
}

/** Interrupt handler for port e. This is where sistem goes on an interrupt call */

void PORTE_IRQHandler()
{
	current_event.current_pin = PORT_GetPinsInterruptFlags(PORTE);
	current_event.current_port = PORTE;
	PORT_ClearPinsInterruptFlags( PORTE, 1 << current_event.current_pin); //Deactivate interrupt
	xSemaphoreGiveFromISR(gpio_event_sem, 0); //Give semaphore to gpio_handler
}

void rtos_gpio_handler()
{
	/** Creates a semaphore */
	gpio_event_sem = xSemaphoreCreateBinary();
	/** Creates the task until it breaks */
	for(;;)
	{
		xSemaphoreTake(gpio_event_sem, portMAX_DELAY); //Waits for signal generated in interrupt
		uint8_t i = 0;
		while(gpio_handles[i].is_init)
		{
			/** Search for the current event pin*/
			uint8_t j = 0;
			while(current_event.current_pin != 1)
			{
				current_event.current_pin >>= 1; //Calculates pin number counting the shifts
				j++;
			}



			if(j == gpio_handles[i].gpio_config.pin_number //If current pin interrupt corresponds to a given handler, activate that task
					&& (uint32_t)current_event.current_port == (uint32_t)get_port_base(gpio_handles[i].gpio_config.port))
			{
				xSemaphoreGive(gpio_handles[i].interrupt_sem);
				break;
			}
		}
	}

}
rtos_gpio_flag_t rtos_gpio_init(rtos_gpio_config_t *config)
{
	/** Looks for the next available handle */
	uint8_t i;
	for(i = 0; i < NUMBER_OF_GPIO_HANDLERS; i++)
	{
		if(!gpio_handles[i].is_init)
		{
			break;
		}
	}

	if(i >= NUMBER_OF_GPIO_HANDLERS)
	{
		return rtos_gpio_fail; //If there are no more handlers, return error
	}

	/** Assigns config and creates a semaphore*/

	gpio_handles[i].handle_number = i;
	gpio_handles[i].gpio_config = *config;
	gpio_handles[i].interrupt_sem = xSemaphoreCreateBinary();

	/** We use the port out of the handles config, just in case this failed */
	enable_port_clock(gpio_handles[i].gpio_config.port); //Enable clock of corresponding port

	PORT_SetPinConfig(get_port_base(gpio_handles[i].gpio_config.port), //Set pin config with given parameters
			gpio_handles[i].gpio_config.pin_number,
			&gpio_handles[i].gpio_config.pin_config);

	/** if the config enabled interrupts, set the handle ass interrupt*/
	if(interrupt_enable == gpio_handles[i].gpio_config.interrupt_flag)
	{
		PORT_SetPinInterruptConfig(get_port_base(gpio_handles[i].gpio_config.port), //Activate interrupt
				gpio_handles[i].gpio_config.pin_number,
				gpio_handles[i].gpio_config.interrupt_type);
	}

	gpio_pin_config_t gpio_input_config =
		{ gpio_handles[i].gpio_config.direction, 1, };

	GPIO_PinInit(get_gpio_base(gpio_handles[i].gpio_config.port),
			gpio_handles[i].gpio_config.pin_number,
			&gpio_input_config);

	gpio_handles[i].is_init = 1; //Indicates that handler is active
	return rtos_gpio_sucess;
}

rtos_gpio_flag_t rtos_gpio_wait(rtos_gpio_config_t *config)
{
	/** Looks for port and pin and puts it to sleep */
	uint8_t i;
	for(i = 0; i < NUMBER_OF_GPIO_HANDLERS; i++)
	{
		if(((gpio_handles[i].gpio_config.port == config->port)
				&& (gpio_handles[i].gpio_config.pin_number == config->pin_number)))
		{
			break;
		}
	}

	/** if it wasn't found returns error*/

	if(i >= NUMBER_OF_GPIO_HANDLERS)
	{
		return rtos_gpio_fail; //If handler not found
	}

	xSemaphoreTake(gpio_handles[i].interrupt_sem, portMAX_DELAY); //Makes the current task to suspend
	return rtos_gpio_sucess;
}

void rtos_create_gpio_handler_task()
{
	xTaskCreate(rtos_gpio_handler, "gpio", 110, NULL, 0, NULL);
}

static inline void enable_port_clock(rtos_gpio_port_t port)
{
	switch(port)
	{
	case rtos_gpio_portA:
		CLOCK_EnableClock(kCLOCK_PortA);
		break;
	case rtos_gpio_portB:
		CLOCK_EnableClock(kCLOCK_PortA);
		break;
	case rtos_gpio_portC:
		CLOCK_EnableClock(kCLOCK_PortA);
		break;
	case rtos_gpio_portD:
		CLOCK_EnableClock(kCLOCK_PortA);
		break;
	case rtos_gpio_portE:
		CLOCK_EnableClock(kCLOCK_PortA);
		break;
	}
}

static inline PORT_Type * get_port_base(rtos_gpio_port_t port)
{
	PORT_Type * port_base = PORTA;
	switch(port)
	{
	case rtos_gpio_portA:
		port_base = PORTA;
		break;
	case rtos_gpio_portB:
		port_base = PORTB;
		break;
	case rtos_gpio_portC:
		port_base = PORTC;
		break;
	case rtos_gpio_portD:
		port_base = PORTD;
		break;
	case rtos_gpio_portE:
		port_base = PORTE;
		break;
	}

	return port_base;
}

static inline GPIO_Type * get_gpio_base(rtos_gpio_port_t port)
{
	GPIO_Type * gpio_base = GPIOA;
	switch(port)
	{
	case rtos_gpio_portA:
		gpio_base = GPIOA;
		break;
	case rtos_gpio_portB:
		gpio_base = GPIOB;
		break;
	case rtos_gpio_portC:
		gpio_base = GPIOC;
		break;
	case rtos_gpio_portD:
		gpio_base = GPIOD;
		break;
	case rtos_gpio_portE:
		gpio_base = GPIOE;
		break;
	}

	return gpio_base;
}
